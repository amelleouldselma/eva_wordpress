<?php
/**
 * Plugin Name: MY AWESOME PLUGIN
 * Plugin URI: https://www.undefined.fr  
 * Description: Display in a page a list of a coordinates recover from a sql table
 * Version: 1.0  
 * Author Name: Amelle 
 * Author: Amelle
 * Author URI: https://amelle_portfolio.surge.sh/
 */

function shortcode_title() {
    return
    "<p>Voici les différentes entreprises dans lesquelles j'ai pu travailler!!</p>";
}
add_shortcode( 'title', 'shortcode_title' );

function shortcode_laListeNom() {

// 1st Method - Declaring $wpdb as global and using it to execute an SQL query statement that returns a PHP object
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT );

            foreach($results as $liste){
                echo "<p>L'entreprise :{$liste -> nom_entreprise} {$liste -> localisation_entreprise} {$liste -> prenom_contact}  {$liste -> nom_contact}  {$liste -> mail_contact}</p>";
            }
}
add_shortcode( 'laListeNom', 'shortcode_laListeNom' );


// //We can use update method to insert data into database.
// $wpdb->update('annuaire', array('id'=>'1', 'name'=>'parvez', array('id' => '2'

//insert()